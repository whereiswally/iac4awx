# IaC For AWX

A set of steps that the candidate needs to follow:
## Read the repo details and understand the guidelines
    #Project - Install [AWX](https://github.com/ansible/awx) in Amazon Web Services using Infrastructure as Code (IaC Method)
        $Preferred underlying network deployment - Terraform
        $Preferred instance deployment method - Terraform
        $Preferred OS - Centos AMI
        $Preferred configuration Management tool - Ansible
        $Preferred containerization method - Docker or docker-compose
    #Technical details to be considered as a minimum:
        $AWS 1xVPC && 1xPublicSubnet && 1xRouteTable && 1xDHCPOptionsSet && 1xInternetGateway
        $Deploy 2xEC2 instances with CentOS Image with default free tier AMI inside the previous VPC IaC
        $EC2 Instace 1 name - AWX-server
        $EC2 Instance 2 name - AWX-Client
        $AWS-EC2 Security Group name - whereiswally
        $AWS-EC2 User Group Name - `whereiswally_group`
        $AWS-EC2 User Name - user1, user2 <Create as many as you need>
        $AWS-EC2 Password to be used (where ever applicable) - P@ssw0rd
        $AWX project repo - Available at the project root with filename  "awx @ 1860a2f7"
        $Terraform tf file structure - Available at the project root folder "infrastructure" & "provisioning" respectively
        $Other deployment details can be used as per the requirement of the project
    #What you need to do
        $Write necessary terraform code depoy the EC2 instances and any confuguration needed to host AWX Server
        $AWX installation steps can be referred from - https://howto.lintel.in/install-ansible-tower-awx-centos-7/ (this is just to understand the generic containerised AWX installation procedure, but you need to account all the changes needed to perform a deployment of AWX over AWS)
        $Configure successful ssh connection from "awx_server" to "awx_client"
        $Configure "awx_client" to receive configuration changes through AWX
    #Project Submission
        $You may want to test the code out at your own AWS personal account if you want to see if the code is running successfully
        $Once done, raise a review request

## Contribute guidelines

Raise a Merge Request in this GitLab repository in order to merge your `feature` branch and commits you have done during your work.

## Present and Demo your code

As important as generating working code it is to demo it and present it to the audience.

Feel free to cover the content that is best to present your work.

Some ideas you might consider:
  * Solution description
  * Logic followed
  * Components & technologies used
  * Execute your & comments on results
  * Challenges and steps followed to overcome
  * Preferred part of the challenge
